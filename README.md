#description

a purple lute, worn on the back of the host

#languages

##4 total

* common (given)
* possessed host
* extra language 1
* extra language 2

#host

30 ft within host to maintain control

##valid hosts

* can be dead (as possessor)
    * possess
        * gain inherent abilities (race)
        * gain natural language
        * constitution, dexterity, etc. remains same as base character
* can be alive (as weapon)
    * augment 
        * tbd

#movement

no movement possible w/o host
##incapacitated
* no strength, no dexterity


#communication

* 100ft speaking through song
* 100ft telepathically communicate

#spellcasting

* can do music casted spells, no movement if no host

#armor

* no armor

#modifiers
* intelligence
* wisdom
* charisma

#race
tiefling (race modifiers)



#homework

* backstory
    * who am I inhabiting
        * rando
* bard school
    * choose a bard school (specialization)
